﻿using ArrowManager.Models;
using ArrowManager.Models.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArrowManager.Utility.Reports
{
    public static class MemberWealthReportFactory
    {
		//TODO: Fill in gaps in data (days metrics were not built) by averaging between good data points
        public static Report buildReport()
        {
			//Setup reports text etc
			Report report = new Report();
			report.columns = new List<Column>();
			report.renderTo = "memberWealthReport";
			report.titleText = "Average Member Wealth Report";
			report.subtitleText = "Per User";
			report.xAxisText = "Day";
			report.yAxisText = "Isk";
			report.xAxisUnits = "Million";
			report.dataTarget = "memberWealthReportData";
			report.dataTypes = getDataTypes();
			report.chartType = ChartType.line;

			//Trying to squeeze more performance out of ef
            UsersContext db = new UsersContext();
			db.Configuration.LazyLoadingEnabled = false;
			db.Configuration.AutoDetectChangesEnabled = false;
			
			//Get stored days from db, and sort them
			var days = db.Days.AsNoTracking().ToList();
            days.Sort((x, y) => DateTime.Compare(x.date, y.date));

            //Set report start date for client-side js
            if (days[0] != null)
               report.startDate = days[0].date;
			
			var memberWealthMetricsMap = new Dictionary<int?, MemberWealthMetrics>();
			var metrics = db.Metrics.AsNoTracking().OfType<MemberWealthMetrics>().ToList();

			//Create a dictionary of dayId's to metric objects
			foreach (var metric in metrics)
			{
				if (!memberWealthMetricsMap.ContainsKey(metric.DayID))
				{
					memberWealthMetricsMap.Add(metric.DayID, metric);
				}
			}
			//For every day, create a column
			foreach (var day in days)
			{
				Column column = new Column();
				column.data = new List<double>();
				column.name = day.date.ToString();
				
				//Get metric for given day
				MemberWealthMetrics metric;
				memberWealthMetricsMap.TryGetValue(day.ID, out metric);

				//Add data to column
				if (metric != null)
					column.data.Add((double)metric.averageMemberWealth);
				report.columns.Add(column);
			}
            return report;
        }

        private static List<ReportDataType> getDataTypes()
        {
            List<ReportDataType> dataTypes = new List<ReportDataType>();
            ReportDataType dataType = new ReportDataType();
            dataType.name = "Average Wealth";
            dataType.visible = true;
            dataTypes.Add(dataType);
            return dataTypes;
        }
       
    }
}