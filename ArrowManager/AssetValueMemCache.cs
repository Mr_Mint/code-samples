﻿using ArrowManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace ArrowManager.Utility.AssetValueMemCache
{
	/// <summary>
	/// In memory asset value cache
	/// </summary>
	public sealed class AssetValueMemCache
	{
		//singleton variables
		private static volatile AssetValueMemCache instance;
		private static object syncRoot = new Object();
		public static AssetValueMemCache Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
							instance = new AssetValueMemCache();
					}
				}

				return instance;
			}
		}

		//Lock variable for updating
		private static object syncAddOrUpdate = new Object();

		//Memcached values
		private Dictionary<int, Tuple<double, DateTime>> assetValueMemCache;

		private AssetValueMemCache()
		{
			//Seed the initial cache from the db
			UsersContext db = new UsersContext();
			assetValueMemCache = db.AssetValueCaches.ToDictionary(x => x.TypeId, x => x.LazyMemCacheTuple);
		}

		/// <summary>
		/// Adds a new or updates an existing assetValueCache in the memCache
		/// </summary>
		/// <param name="value">Value to be added/updated</param>
		public void AddOrUpdateValue(AssetValueCache value)
		{
			lock (syncAddOrUpdate)
			{
				//Check if already in memCache
				if (assetValueMemCache.ContainsKey(value.TypeId))
				{
					//If so, update the entry
					assetValueMemCache[value.TypeId] = value.LazyMemCacheTuple;
				}
				else
				{
					//Doesn't exist, add new entry to dictionary
					assetValueMemCache.Add(value.TypeId, value.LazyMemCacheTuple);
				}
			}
		}

		/// <summary>
		/// Retrieves the value for a single typeId
		/// </summary>
		/// <param name="typeId">The type to retrieve a value for</param>
		/// <param name="checkDisk">Check the diskCache for updates if not in memCache</param>
		/// <returns>Currently stored values for assets, including stale data if present.</returns>
		public double GetAssetValue(int typeId, bool checkDisk = true, int userId = -1)
		{
			//Send to GetAssetValue
			var value = GetAssetValue(new List<int> { typeId }, checkDisk,userId);
			double assetValue;

			//Ensure a value was actually found
			return value.TryGetValue(typeId, out assetValue) == true ? assetValue : 0;
		}

		/// <summary>
		/// Bulk retrieves values from the memCache, and requests updates to values if stale.
		/// </summary>
		/// <param name="typeIds">The typeId's to retrieve values for</param>
		/// <param name="checkDisk">Check the diskCache for updates if not in memCache</param>
		/// <returns>Returns currently stored values for assets, including stale data if present.</returns>
		public Dictionary<int, double> GetAssetValue(IEnumerable<int> typeIds, bool checkDisk = true, int userId = -1)
		{

			var assetValues = new Dictionary<int, double>();
			var assetsStaleOrNotInMemCache = new List<int>();

			foreach (var typeId in typeIds)
			{
				//Check if typeId is in memCache
				if (assetValueMemCache.ContainsKey(typeId))
				{
					//Ensure not already in assetValues(duplicates in input)
					if (!assetValues.ContainsKey(typeId))
					{
						assetValues.Add(typeId, assetValueMemCache[typeId].Item1);
					}
					//Check if stale
					if (assetValueMemCache[typeId].Item2.Subtract(DateTime.UtcNow) >= AssetValueCache.AssetValueExpireTime)
					{
						//Add to list of types to be updated
						assetsStaleOrNotInMemCache.Add(typeId);
					}
				}
				else
				{
					//TODO: not include at all and let callers handle?
					//Not in memCache, set default value of 0
					assetValues.Add(typeId, 0);

					//Add to list of types to be updated
					assetsStaleOrNotInMemCache.Add(typeId);

				}
			}

			//Ensure that we have stale/not found values before checking diskCache
			if (assetsStaleOrNotInMemCache.Count > 0)
			{
				//If checkdisk is enabled, pass the not found/stale typeIds to the diskCache
				if (checkDisk)
				{
					//Foreach value returned by the diskCache
					foreach (var keyPair in AssetValueCache.GetAssetValue(assetsStaleOrNotInMemCache,userId))
					{
						//Ensure key not already in values found by memCache
						if (!assetValues.ContainsKey(keyPair.Key))
						{
							assetValues.Add(keyPair.Key, keyPair.Value);
						}
						else
						{
							//Replace memCaches result with diskCaches
							assetValues[keyPair.Key] = keyPair.Value;
						}
					}
				}
				else
				{
					//Send the not found/stale typeId's to the diskCache async 
					ThreadPool.QueueUserWorkItem(delegate
					{
						AssetValueCache.GetAssetValue(assetsStaleOrNotInMemCache, userId);
					});
				}
			}
			//Return the typeIds and there values
			return assetValues;
		}
	}
}