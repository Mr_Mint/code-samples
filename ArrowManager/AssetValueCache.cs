﻿using ArrowManager.Hubs;
using ArrowManager.Utility;
using ArrowManager.Utility.AssetValueMemCache;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using System.Web;
using WebMatrix.WebData;

namespace ArrowManager.Models
{
	//TODO:
	//1. Utilize delegates to decouple process of updating users.
	//2. Potentially reduce # of conversions between containers?
	//3. Update multithreading to use async/task pattern

	/// <summary>
	/// A disk based value cache
	/// -Backed by Mssql db
	/// -Thread-safe.
	/// -Non-Blocking.
	/// -Returns both fresh and stale data.
	/// </summary>
	[Table("AssetValueCache")]
	public class AssetValueCache
	{
		public static readonly TimeSpan AssetValueExpireTime = new TimeSpan(12, 0, 0);
		private static Object LOCK = new Object();

		[Key]
		[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
		public int ID { get; set; }

		public int TypeId { get; set; }
		public DateTime LastUpdated { get; set; }
		public double ProductValue { get; set; }

		/// <summary>
		/// LazyMemCacheTuple's backer
		/// </summary>
		private Lazy<Tuple<double, DateTime>> lazyMemCacheTuple;

		/// <summary>
		/// A lazy loaded helper property for the memCache
		/// </summary>
		public Tuple<double, DateTime> LazyMemCacheTuple
		{
			get
			{
				return lazyMemCacheTuple.Value;
			}
		}

		public AssetValueCache()
		{
			//Assign a delegate to be used for initializing lazyMemCacheTupleHelper
			lazyMemCacheTuple = new Lazy<Tuple<double, DateTime>>(() =>
			{
				return new Tuple<double, DateTime>(ProductValue, LastUpdated);
			});
		}

		/// <summary>
		/// Retrieves the value for a single typeId
		/// </summary>
		/// <param name="typeId">The type to retrieve a value for</param>
		/// <returns>Currently stored values for assets, including stale data if present.</returns>
		public static double GetAssetValue(int typeId, int userId = -1)
		{
			var value = GetAssetValue(new List<int> { typeId }, userId);
			double assetValue;
			return value.TryGetValue(typeId, out assetValue) == true ? assetValue : 0;
		}

		/// <summary>
		/// Bulk retrieves asset values from the db, and requests updates to values if stale.
		/// </summary>
		/// <param name="typeIds">The typeId's to retrieve values for</param>
		/// <param name="session">Session to update client upon data refresh</param>
		/// <returns>Currently stored values for assets, including stale data if present.</returns>
		public static Dictionary<int, double> GetAssetValue(List<int> typeIds, int userId = -1)
		{
			//Values we will return
			var assetValues = new Dictionary<int, double>();

			if (typeIds.Count == 0)
			{
				return assetValues;
			}

			//Transform lists into hashsets for O(1) compare
			var assetsIn = new HashSet<int>(typeIds);
			UsersContext db = new UsersContext();
			var assetValuesCached = db.AssetValueCaches.ToDictionary(x => x.TypeId);


			//Compare sets to determine what data is stale
			var typeIdValuesCached = assetsIn.Intersect(assetValuesCached.Keys);
			var typeIdValuesNotCached = assetsIn.Except<int>(typeIdValuesCached);
			var typeIdValuesNeedingUdpate = new List<int>();

			//Value has not been cached yet, return 0 and add to refresh queue
			foreach (var typeId in typeIdValuesNotCached)
			{
				assetValues.Add(typeId, 0);
				typeIdValuesNeedingUdpate.Add(typeId);
			}

			//Value is in cache
			foreach (var assetTypeId in typeIdValuesCached)
			{
				//Check if stale
				if (assetValuesCached[assetTypeId].LastUpdated.Subtract(DateTime.UtcNow) >= AssetValueExpireTime)
				{
					typeIdValuesNeedingUdpate.Add(assetTypeId);
				}
				//set value regardless of fresh or stale
				assetValues.Add(assetTypeId, assetValuesCached[assetTypeId].ProductValue);
			}

			//Create thread to update asset values if required
			if (typeIdValuesNeedingUdpate.Count > 0)
			{
				ThreadPool.QueueUserWorkItem(delegate
				{
					if (userId != -1)
					{
						//Alert user via websocket notification that some values are not up to date/present
						Broadcaster.Instance.SendNotificationMessage(userId, new Message()
						{
							Type = MessageType.warning,
							Title = "Asset Value Notification",
							Text = String.Format("{0} asset values appear to be out of date, updating now!", typeIdValuesNeedingUdpate.Count)
						});
					}
					var result = UpdateAssetValues(typeIdValuesNeedingUdpate, userId);

				});
			}
			//Return fresh data and stale data
			return assetValues;
		}

		/// <summary>
		/// Updates values for given typeIds.
		/// </summary>
		/// <param name="typeIds">TypeIds to be updated.</param>
		/// <param name="userId">UserId to send notification to.</param>
		/// <returns></returns>
		private static bool UpdateAssetValues(List<int> typeIds, int userId = -1)
		{
			//Lock to avoid creating duplicates in the cache as multiple threads can hit this
			lock (LOCK)
			{
				//Convert to hash set for increased compare speed
				var assetsIn = new HashSet<int>(typeIds);

				UsersContext db = new UsersContext();
				var assetValuesCached = db.AssetValueCaches.ToList();

				//Determine what is and isn't cached/stale
				var typeIdValuesCached = new HashSet<int>(assetValuesCached.ConvertAll<int>(x => x.TypeId));
				var typeIdValuesNotCached = assetsIn.Except<int>(typeIdValuesCached);
				var typeIdValuesNeedRefresh = assetsIn.Except<int>(typeIdValuesNotCached);

				//Create all non stored asset values, update them, save to db
				foreach (var typeId in typeIdValuesNotCached)
				{
					//Create and add to cache
					var assetValueCacheEntry = new AssetValueCache()
					{
						ProductValue = APIUtilities.EveItemHelpers.Instance.GetProductValue(typeId),
						TypeId = typeId,
						LastUpdated = DateTime.UtcNow
					};

					//Update the memCache
					AssetValueMemCache.Instance.AddOrUpdateValue(assetValueCacheEntry);

					//Add and save to db
					db.AssetValueCaches.Add(assetValueCacheEntry);
					db.SaveChanges();
				}

				//Refresh already stored asset values that are stale
				foreach (var assetValueCacheEntry in assetValuesCached.Where(x => typeIdValuesNeedRefresh.Contains(x.TypeId)))
				{
					//Make sure it is still stale
					if (assetValueCacheEntry.LastUpdated.Subtract(DateTime.UtcNow) >= AssetValueExpireTime)
					{
						//Update values
						assetValueCacheEntry.ProductValue = APIUtilities.EveItemHelpers.Instance.GetProductValue(assetValueCacheEntry.TypeId);
						assetValueCacheEntry.LastUpdated = DateTime.UtcNow;
					}

					//Update the memCache
					AssetValueMemCache.Instance.AddOrUpdateValue(assetValueCacheEntry);

					//Save the changes to the db
					db.SaveChanges();

				}

				if (userId != -1)
				{
					//Update user via websocket notification that asset values have been updated
					Broadcaster.Instance.SendNotificationMessage((int)userId, new Message()
					{
						Type = MessageType.success,
						Title = "Asset Value Notification",
						Text = "All requested asset values are up to date, refresh to view!"
					});
				}

				return true;
			}
		}
	}
}