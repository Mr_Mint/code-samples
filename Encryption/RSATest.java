package rsaAssignment;

import java.util.*;
import java.math.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// A simple RSA implementation
public class RSATest {
	// These variable would be private (some destroyed such as p & q) in a real scenario
	public BigInteger p, q;
	public BigInteger n;
	private BigInteger phi;
	public BigInteger e, d;
	private BigInteger minDifference;
	long startTime;
	long endTime;
	long keyGenerationDuration, encyrptionDuration, decryptionDuration;

	public RSATest(int primeLength) {
		startTime = System.nanoTime();
		GenerateKeys(primeLength);
		endTime = System.nanoTime();
		keyGenerationDuration = endTime - startTime;
	}

	public void GenerateKeys(int primeLength) {
		System.out.println("Generating Keys, Please Wait...");
		/* Pick e */
		e = new BigInteger("65537");
		/* Select two large prime numbers. */
		p = BigInteger.probablePrime(primeLength, new Random());
		q = BigInteger.probablePrime(primeLength, new Random());

		/* Calculate n = p.q */
		n = p.multiply(q);

		/* Calculate phi = (p - 1).(q - 1) */
		phi = p.subtract(BigInteger.valueOf(1));
		phi = phi.multiply(q.subtract(BigInteger.valueOf(1)));

		while (e.gcd(phi).compareTo(BigInteger.valueOf(1)) != 0) {
			e = e.add(BigInteger.ONE);
			if (e.compareTo(phi) == 1) {
				// Can't Find Keys, try again
				this.GenerateKeys(primeLength);
			}

		}
		/* Calculate d such that e.d = 1 (mod phi) */
		d = e.modInverse(phi);
	}
	
	//Encrypt Message
	public BigInteger encrypt(BigInteger plaintext) {
		return plaintext.modPow(e, n);
	}
	
	//Decrypt Message
	public BigInteger decrypt(BigInteger ciphertext) {
		return ciphertext.modPow(d, n);
	}

	public static void main(String[] args) throws IOException {

		BigInteger message, encryptedMessage, decryptedMessage;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		//Request bit length of message to be generated and encoded
		System.out.println("Enter bit length of Randomly Generated Message to be encrypted (2-2048): ");
		int messageLength = Integer.parseInt(br.readLine());
		
		//Generate keys, ensure bit lengths are within range to avoid long compute time
		RSATest app = new RSATest(Math.min(Math.max(messageLength, 512),2048));
		//Create Message
		message = new BigInteger(Math.min(Math.max(messageLength, 2), 2048), 1,new Random());
		System.out.println("\n--------------Messages---------------\n");
		System.out.println("Message before Encryption: " + message.toString() + "\n");
		
		//Encrypt Message
		app.startTime = System.nanoTime();
		encryptedMessage = app.encrypt(message);
		app.endTime = System.nanoTime();
		app.encyrptionDuration = app.endTime - app.startTime;
		//Display Encrypted Message
		System.out.println("Encrpyted Message: " + encryptedMessage.toString() + "\n");

		//Decrypt the Encrypted Message
		app.startTime = System.nanoTime();
		decryptedMessage = app.decrypt(encryptedMessage);
		app.endTime = System.nanoTime();
		app.decryptionDuration = app.endTime - app.startTime;
		//Display decrypted Message
		System.out.println("Message after Decryption : " + decryptedMessage.toString() + "\n");
		
		//Check that the messages pre and post encryption/decryption match
		if (message.toString().equals(decryptedMessage.toString()))
			System.out.println("Encryption/Decryption: Succesfull!");
		else
			System.out.println("Encryption/Decryption: Unsuccesfull");
		
		//Display Performance Metrics
		System.out.println("\n--------------Performance Metrics---------------\n");
		System.out.println("Key Generation Time Required: "
				+ (double) app.keyGenerationDuration / 1000000.0
				+ " milliseconds\n");
		System.out.println("Encryption Time Required: "
						+ (double) app.encyrptionDuration / 1000000.0
						+ " milliseconds\n");
		System.out.println("Decryption Time Required: "
						+ (double) app.decryptionDuration / 1000000.0
						+ " milliseconds\n");
		System.out.println("Total Time: "
				+ (double) (app.keyGenerationDuration + app.encyrptionDuration + app.decryptionDuration) / 1000000000.0
				+ " seconds\n");
		
		//Display Keys
		System.out.println("\n--------------Generated Keys---------------\n");
		System.out.println("p & q length: " + app.p.bitLength() +" bits" + "\n");
		System.out.println("p Value: " + app.p + "\n");
		System.out.println("q Value: " + app.q + "\n");
		System.out.println("n Value: " + app.n + "\n");
		System.out.println("e Value: " + app.e + "\n");
		System.out.println("d Value: " + app.d + "\n");
	}
}